#/bin/bash

# run as: ./gender_rank <<FILE: e.g. rank_gender_race.txt>>


# PART I: Selecting relevant data and structuring it neatly

# Header:
echo "RANKS:, O01, O02, O03, O04, O05, O06, O07, O08, O09, O10" > gender_rank.csv

# Row, total of male:
echo -n "MALE:," >> gender_rank.csv
sed -n '302, 375p' $1 | sed -n '50p' | \
cut -d',' -f4-13 | tr -d '"'>> gender_rank.csv

# Row, total of female:
echo -n "FEMALE:," >> gender_rank.csv
sed -n '302, 375p' $1 | sed -n '74p' | \
cut -d',' -f4-13 | tr -d '"' >> gender_rank.csv
echo >> gender_rank.csv

# Row, total of total:
echo -n "TOTAL:," >> gender_rank.csv
sed -n '302, 375p' $1 | sed -n '26p' | \
cut -d',' -f4-13 | tr -d '"' >> gender_rank.csv



# Part II: computing results (rank (higher or lower) and gender (male or female):

echo 'Computed results:'
echo '(Lower ranks: O01-O05, higher ranks: O06-O10)'
echo

echo 'Total lower ranks'
TOT_LOW=`grep 'TOTAL' gender_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc` 
echo $TOT_LOW
echo

echo 'Total higher ranks'
TOT_HIG=`grep 'TOTAL' gender_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`
echo $TOT_HIG
echo

echo 'Male lower ranks:'
MAL_LOW=`grep -w 'MALE' gender_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`
echo $MAL_LOW
echo

echo 'Male higher ranks:'
MAL_HIG=`grep -w 'MALE' gender_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`
echo $MAL_HIG
echo

echo 'Female lower ranks:'
FEM_LOW=`grep 'FEMALE' gender_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`
echo $FEM_LOW
echo

echo 'Female higher ranks:'
FEM_HIG=`grep 'FEMALE' gender_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`
echo $FEM_HIG
echo

echo '--------------------'
echo

echo 'Percentage of females in lower ranks:'
LOW_PER=`echo "$FEM_LOW / $TOT_LOW * 100" | bc -l`
echo $LOW_PER
echo

echo 'Percentage of females in higher ranks:'
HIG_PER=`echo "$FEM_HIG / $TOT_HIG * 100" | bc -l`
echo $HIG_PER
echo

# PART III: optionally, remove the following hash to delete the csv file:
#rm gender_rank.csv
