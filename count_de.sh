#/bin/bash

# run as: ./count_de.sh <<file to be analyzed, i.e. RUG_wiki_page.txt>>

# Count the word "de", including capital letters:
echo 'Including capital letters:'
cat $1 | grep -woi 'de' | wc -l


# Count the word "de", not including capital letters:
echo 'Not including capital letters:'
cat $1 | grep -wo 'de' | wc -l

