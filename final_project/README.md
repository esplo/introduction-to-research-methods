# Diversity within U.S. military officer ranks

This the code for my final project for the course 'Introduction to Research Methods'. For this project, I carried out a small research focussing on diversity (gender, ethnicity) within officer ranks in the U.S. military. In this README file, you will find everything needed to reproduce the experiments I conducted.


## Performing the experiments

#### Data

The scripts that I wrote (gender_rank.sh and race_rank.sh) use data from a dataset provided by the U.S. Department of Defense. This dataset is public and falls under the U.S. Government Works license, which means 'there is generally no copyright restriction on reproduction, derivative works, distribution, performance or display'.

The dataset can be downloaded via the following link:
https://catalog.data.gov/dataset/personnel-trends-by-gender-race


### Preprocessing data
All commands mentioned are to be performed in the shell. For details on the shell and system the tests were originally performed on, please see the 'Versioning' section below.

In order to be able to extract relevant data, we converted the data to a .txt file in the following manner:

```
xls2csv rgr.xls > rank_gender_race.txt
```
In the above example, rgr.xls is the dataset that was downloaded from the U.S. Department of Defense. Using a csv converter to convert a file into a .txt file may seem questionable, but this method turned out to work best during the research process.

Note: if you do not yet have installed xls2csv, you can do so by running the following command:
```
sudo apt install catdoc
```

For the purpose of this project, I added rank_gender_race.txt to this repository so reproducing the results would be easier and faster.

### Running the scripts

Once the rank_gender_race.txt file is created, the next step is running the actual scripts on this data.

For results concerning gender and rank, use the following command:

```
./gender_rank.sh rank_gender_race.txt
```


For results concerning race and rank, use the following command:

```
./race_rank.sh rank_gender_race.txt
```

Note: in order to run these commands, permissions may have to be set right. This can be done with the following commands:

```
chmod +x gender_rank.sh
```


```
chmod +x race_rank.sh
```

Running the scripts will provide you with a list of results in the terminal. Additionally, it will provide csv files with the relevant data from the dataset. If you do not want these csv files, you can remove the '#' in the last line of the files. For more information, see the documentation within the scripts itself.

## Versioning
The experiments were run on a virtual box with Ubuntu 18.04.2 LTS.

Because in order to ensure replicability even small details may be relevant, we present other information about the system the tests were originally performed on:

Processor: Intel Core i5-4210U CPU @ 1.72GHz x 2
Graphics: llvmpipe (LLVM 7.0, 256 bits)
GNOME: 3.28.2
OS type: 32-bit
Virtualization: Oracle
Disk: 20,0 G
