#/bin/bash

# run as: ./race_rank <<FILE: e.g. rank_gender_race.txt>>


# PART I: Selecting relevant data and structuring it neatly

# Header:
echo "RANKS:, O01, O02, O03, O04, O05, O06, O07, O08, O09, O10" > race_rank.csv

# Row, total of 'AMERICAN INDIAN / ALASKAN NATIVE'
echo -n "AMERICAN INDIAN / ALASKAN NATIVE:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '5p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'ASIAN'
echo -n "ASIAN:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '8p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'BLACK'
echo -n "BLACK:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '11p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'MULTI RACE'
echo -n "MULTI RACE:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '14p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'NATIVE HAWAIIAN / PACIFIC ISLANDER'
echo -n "NATIVE HAWAIIAN / PACIFIC ISLANDER:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '17p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'WHITE':
echo -n "WHITE:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '20p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'UNKNOWN':
echo -n "UNKNOWN:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '23p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv

# Row, total of 'TOTAL':
echo -n "TOTAL:," >> race_rank.csv
sed -n '302, 375p' $1 | sed -n '26p' | \
cut -d',' -f4-13 | tr -d '"' >> race_rank.csv


# Part II: computing results:


echo 'Computed results:'
echo '(Lower ranks: O01-O05, higher ranks: O06-O10)'
echo '(Total = Total - Unknown)'
echo '(Minorities = AMI/ALN, ASI, BLA, MUL, P/I)'
echo

# II A) Calculating totals per ranks (lower or higher) and race

LOW_AMI=`grep 'AMERICAN INDIAN / ALASKAN NATIVE' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_AMI=`grep 'AMERICAN INDIAN / ALASKAN NATIVE' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_ASIAN=`grep 'ASIAN' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_ASIAN=`grep 'ASIAN' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_BLACK=`grep 'BLACK' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_BLACK=`grep 'BLACK' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_MULTI=`grep 'MULTI RACE' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_MULTI=`grep 'MULTI RACE' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_PAC=`grep 'PACIFIC ISLANDER' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_PAC=`grep 'PACIFIC ISLANDER' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_WHITE=`grep 'WHITE' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_WHITE=`grep 'WHITE' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_UNKNOWN=`grep 'UNKNOWN' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_UNKNOWN=`grep 'UNKNOWN' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

LOW_TOTAL=`grep 'TOTAL' race_rank.csv | cut -d',' -f2-6 | tr ',' '+' | bc`

HIGH_TOTAL=`grep 'TOTAL' race_rank.csv | cut -d',' -f7-11 | tr ',' '+' | bc`

# II B) Calculating totals for relevant information
#       (rank (higher or lower) and race (white or minority)

echo 'Total lower white:'
echo $LOW_WHITE
echo

echo 'Total higher white:'
echo $HIGH_WHITE
echo

echo "Lower ranks minorities:"
LOW_MIN=`echo $LOW_AMI + $LOW_ASIAN + $LOW_BLACK + $LOW_MULTI + $LOW_PAC | bc`
echo $LOW_MIN
echo

echo "Higher ranks minorities:"
HIGH_MIN=`echo $HIGH_AMI + $HIGH_ASIAN + $HIGH_BLACK + $HIGH_MULTI + $HIGH_PAC | bc`
echo $HIGH_MIN
echo

echo 'Total lower ranks:'
LOW_TOT=`echo $LOW_TOTAL - $LOW_UNKNOWN | bc`
echo $LOW_TOT
echo

echo 'Total higher ranks:'
HIGH_TOT=`echo $HIGH_TOTAL - $HIGH_UNKNOWN | bc`
echo $HIGH_TOT
echo


echo '--------------------'
echo

echo 'Percentage of minorities in lower ranks:'
LOW_PER=`echo "$LOW_MIN / $LOW_TOT * 100" | bc -l`
echo $LOW_PER
echo

echo 'Percentage of minorities in higher ranks:'
HIG_PER=`echo "$HIGH_MIN / $HIGH_TOT * 100" | bc -l`
echo $HIG_PER
echo

# PART III: optionally, remove the following hash to delete the csv file:
#rm race_rank.csv
